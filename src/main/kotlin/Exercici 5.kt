import java.util.Scanner

/*
* Lorenzo Poderoso Dalmau
* Exercici 4 : RepeatedAnswer
 */


fun main() {
    val scanner = Scanner(System.`in`)
    val card = mutableListOf<Int>()
    var remaining = 10

    askForCardNumbers(scanner, card)

    askForCalledNumbers(scanner, card, remaining)

    if (remaining == 0) {
        println("¡BINGO!")
    }
}

fun askForCardNumbers(scanner: Scanner, card: MutableList<Int>) {
    println("Introduce los 10 números de la tarjeta:")
    for (i in 1..10) {
        val number = scanner.nextInt()
        card.add(number)
    }
}

fun askForCalledNumbers(scanner: Scanner, card: MutableList<Int>, remaining: Int) {
    var remainingNumbers = remaining
    while (remainingNumbers > 0) {
        print("Introduce el número que se ha cantado o END para salir: ")
        val input = scanner.next()
        if (input == "END") {
            break
        }
        val number = input.toInt()
        if (number in card) {
            card.remove(number)
            remainingNumbers--
            println("¡Queda(n) $remainingNumbers número(s)!")
        } else {
            println("Ese número no está en la tarjeta. Inténtalo de nuevo.")
        }
    }
}
