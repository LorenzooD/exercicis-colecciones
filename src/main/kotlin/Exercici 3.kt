import java.util.*


/*
* Lorenzo Poderoso Dalmau
* Exercici 3 : SchoolDelegatVoting
 */


fun main() {
    val scanner = Scanner(System.`in`)
    val votes = mutableMapOf<String, Int>()

    getVotes(scanner, votes)
    printVoteCount(votes)
}

fun getVotes(scanner: Scanner, votes: MutableMap<String, Int>) {
    var input: String
    println("Introduce el nombre del estudiante a votar o END para terminar:")
    do {
        input = scanner.nextLine().trim()
        if (input != "END") {
            if (votes.containsKey(input)) {
                votes[input] = votes[input]!! + 1
            } else {
                votes[input] = 1
            }
        }
    } while (input != "END")
}

fun printVoteCount(votes: Map<String, Int>) {
    println("Resultado de la votación:")
    for ((name, count) in votes) {
        println("$name: $count")
    }
}
