import java.util.Scanner

/*
* Lorenzo Poderoso Dalmau
* Exercici 2 : RoadSigns
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val cartells = leerCartells(scanner)

    print("Introduce el número de consultas: ")
    val numConsultes = scanner.nextInt()

    val resultados = mutableListOf<String>()
    for (i in 1..numConsultes) {
        print("Introduce el metro de la consulta $i: ")
        val consulta = scanner.nextInt()
        val resultado = consultarCartell(cartells, consulta)
        resultados.add(resultado)
    }

    mostrarResultados(resultados)
}

fun leerCartells(scanner: Scanner): List<Pair<Int, String>> {
    val cartells = mutableListOf<Pair<Int, String>>()
    print("Introduce el número de cartells: ")
    val numCartells = scanner.nextInt()
    scanner.nextLine()
    for (i in 1..numCartells) {
        println()
        print("Introduce el metro y el texto del cartell $i: ")
        val metro = scanner.nextInt()
        val text = scanner.nextLine().trim()
        cartells.add(metro to text)
    }
    return cartells
}

fun consultarCartell(cartells: List<Pair<Int, String>>, consulta: Int): String {
    val cartell = cartells.findLast { it.first <= consulta }
    return cartell?.second ?: "no hi ha cartell"
}

fun mostrarResultados(resultados: List<String>) {
    println()
    println("Resultados de las consultas:")
    resultados.forEach { println(it) }
}

