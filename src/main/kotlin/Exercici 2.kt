import java.util.*


/*
* Lorenzo Poderoso Dalmau
* Exercici 2 : EmployeeByID
 */

data class Empleado(val dni: String, val nombre: String, val apellidos: String, val direccion: String)

fun main() {
    val empleados = mutableListOf<Empleado>()
    val scanner = Scanner(System.`in`)

    // Aquí pedirá al usuario que introduzca el número de empleados
    print("Introduce el número de empleados: ")
    val numEmpleados = scanner.nextInt()
    scanner.nextLine()

    // Aquí que introduzca la información de cada empleado
    for (i in 1..numEmpleados) {
        print("Introduce el DNI del empleado $i: ")
        val dni = scanner.nextLine().trim()
        print("Introduce el nombre del empleado $i: ")
        val nombre = scanner.nextLine().trim()
        print("Introduce los apellidos del empleado $i: ")
        val apellidos = scanner.nextLine().trim()
        print("Introduce la dirección del empleado $i: ")
        val direccion = scanner.nextLine().trim()
        empleados.add(Empleado(dni, nombre, apellidos, direccion))
    }

    // Pedimos al usuario que introduzca el DNI del empleado a buscar
    while (true) {
        print("Introduce el DNI del empleado a buscar (o END para terminar): ")
        val dniBusqueda = scanner.nextLine().trim()
        if (dniBusqueda == "END") break

        // Buscamos el empleado por DNI y mostramos sus datos
        val empleado = buscarEmpleadoPorDNI(empleados, dniBusqueda)
        if (empleado != null) {
            println("${empleado.nombre} ${empleado.apellidos} - ${empleado.dni}, ${empleado.direccion}")
        } else {
            println("Empleado no encontrado")
        }
    }
}

fun buscarEmpleadoPorDNI(empleados: List<Empleado>, dniBusqueda: String): Empleado? {
    return empleados.find { it.dni == dniBusqueda }
}
