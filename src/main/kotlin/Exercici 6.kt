import java.util.*

/*
* Lorenzo Poderoso Dalmau
* Exercici 4 : RepeatedAnswer
 */

interface GymControlReader {
    fun nextId() : String
}

class GymControlManualReader(val scanner:Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()
}

fun main() {
    val reader: GymControlReader = GymControlManualReader()
    val enteredUsers = mutableSetOf<String>()
    readAndProcessIds(reader, enteredUsers, 8)
}

fun readAndProcessIds(reader: GymControlReader, enteredUsers: MutableSet<String>, numIds: Int) {
    for (i in 1..numIds) {
        val id = reader.nextId()
        if (enteredUsers.contains(id)) {
            enteredUsers.remove(id)
            println("$id ha salido")
        } else {
            enteredUsers.add(id)
            println("$id ha entrado")
        }
    }
}
