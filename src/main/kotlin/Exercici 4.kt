import java.util.*

/*
* Lorenzo Poderoso Dalmau
* Exercici 4 : RepeatedAnswer
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val answers = mutableSetOf<String>()
    var input = ""

    // Pide las respuestas y las almacena en un conjunto
    println("Introduce la respuesta o END para terminar:")
    while (input != "END") {
        input = scanner.nextLine().trim()
        if (input != "END") {
            if (!answers.add(input)) {
                println("MEEEC!")
            }
        }
    }
}
